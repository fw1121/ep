args=commandArgs(TRUE)

red="indianred"
blue="royalblue3"
purple="darkmagenta"
grey="gray61"
green="darkseagreen3"
lightblue="darkslategray3"

setwd(args[1])
prefix=args[2]
titleName=args[3]





A.qual=read.table(args[4])
A.qual.sum=colSums(A.qual)

C.qual=read.table(args[5])
C.qual.sum=colSums(C.qual)

G.qual=read.table(args[6])
G.qual.sum=colSums(G.qual)

T.qual=read.table(args[7])
T.qual.sum=colSums(T.qual)

Ins.qual=read.table(args[8])
Ins.qual.sum=colSums(Ins.qual)

Del.qual=read.table(args[9])
Del.qual.sum=colSums(Del.qual)

N.qual=read.table(args[10])
N.qual.sum=colSums(N.qual)

x=(1:50)

plotName = gsub(" ","",paste(prefix,"_qualProfile.jpeg"),fixed=TRUE)

jpeg(file=plotName)
boxplot(list(rep(x,A.qual.sum),rep(x,C.qual.sum),rep(x,G.qual.sum),rep(x,T.qual.sum),rep(x,Ins.qual.sum),rep(x,Del.qual.sum),rep(x,N.qual.sum)),col=c(grey,purple,red,blue,lightblue,green,"black"), names=c("Subst. of A","Subst. of C","Subst. of G","Subst. of T","Insertions","Deletions","N"), las=2, ylab="Quality scores",main="Boxplot of quality values for different types of errors")
dev.off()


