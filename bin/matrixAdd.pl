#!/usr/bin/perl

#Usage ./matrixAdd.pl nr_rows nr_col matrix1 matrix2 output_file

use strict; use warnings; use diagnostics;

my $nr_rows = $ARGV[0];
my $nr_cols = $ARGV[1];
my $matrix1 = $ARGV[2];
my $matrix2 = $ARGV[3];
my $output = $ARGV[4];

open(FILE_m1, $matrix1) or die "Can't open m1\n";
open(FILE_m2, $matrix2) or die "Can't open m2\n";

my @subM1 = (0) x $nr_cols;
my @subM2 = (0) x $nr_cols;

my @M1 = (\@subM1);
my @M2 = (\@subM2);

my $i=0;

for ($i=1;$i<$nr_rows;$i++){
	my @subM1 = (0) x $nr_cols;
	push @M1, \@subM1;
	my @subM2 = (0) x $nr_cols;
	push @M2, \@subM2;
}

my $k=0;
my $j=0;

my $line1 = 0;
my $line2 = 0;

my @temp=();

while($line1 = <FILE_m1>){
	chomp($line1);
	@temp=split("\t",$line1);
	for($j=0;$j<$nr_cols;$j++){
		$M1[$k][$j]=$temp[$j];
	}
	$k++;
}



$k=0;
$j=0;

while($line2 = <FILE_m2>){
        chomp($line2);
        @temp=split("\t",$line2);
        for($j=0;$j<$nr_cols;$j++){
                $M2[$k][$j]=$temp[$j];
        }
        $k++;
}


#for($k=0;$k<$nr_rows;$k++){
#	for($j=0;$j<$nr_cols;$j++){
#		print $M1[$k][$j]."\t";
#	}
#	print "\n";
#}

#print "\n\n";

#for($k=0;$k<$nr_rows;$k++){
#        for($j=0;$j<$nr_cols;$j++){
#                print $M2[$k][$j]."\t";
#        }
#        print "\n";
#}



for($k=0;$k<$nr_rows;$k++){
       for($j=0;$j<$nr_cols;$j++){
               $M1[$k][$j]=$M1[$k][$j]+$M2[$k][$j];
       }
}

#print "\n\n";

my $FILE_OUT;

open($FILE_OUT, '>', $output) or die "Can't open output file.\n\n";

#print "Writing output to file $output.\n\n";

for($k=0;$k<$nr_rows;$k++){
        for($j=0;$j<$nr_cols;$j++){
                print $FILE_OUT $M1[$k][$j]."\t";
        }
        print $FILE_OUT "\n";
}





close($FILE_OUT);
close(FILE_m1);
close(FILE_m2);


